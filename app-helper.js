module.exports = {
	API_URL : process.env.NEXT_PUBLIC_API_URL,
	toJSON : (response) => response.json(),
	getAccessToken : () => global.window && localStorage.getItem('token')
}



// env
// NEXT_PUBLIC_API_URL=http://localhost:4000/api
// -> will expose the data that made ---

// NEXT_PUBLIC_API_URL=http://localhost:4000/api

