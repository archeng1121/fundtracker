// lets acquire the components needed in order to build this new file
import { Container } from 'react-bootstrap';

// lets learn about using "HEAD" component
import Head from 'next/head'

// describe the anatomy of this component
// describe the prop that we want o pass down inside the component
const View = ({title, children}) => {
	return(
		<>
			<Head>
				<title key="title-tag">{title}</title>
				<meta key="title-meta" name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<Container className="mt-5 pt-4 mb-5">
				{children}
			</Container>
		</>
	)
}

export default View;