import { Navbar } from 'react-bootstrap';
// import Link from 'next/link'

export default function Footer() {
    
    return(

        <Navbar fixed="bottom" variant="dark" bg="dark" className="footerTo">
  			<div className="col-md-12 text-center footerTo">	
  				MoneyTrack | aRc &copy; 2021
  			</div>
		</Navbar>
        
    )
} 
