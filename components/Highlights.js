
import { Container , Card , Row , Col } from 'react-bootstrap';

// identify the proceduresin order to build this page.
const Highlights = () => {
	// describe the anatomy of the element inside the return scope.
	return (
		<Container className="mt-5 pt-5">
			<Row>
				{/*first card*/}
				<Col xs md="12">
				<h2 className="sub">Welcome to</h2>
				<h1 className="landing">MoneyTrack!</h1>
				<h2 className="sub">where tracking your money made easier!</h2>
					
				</Col>
				
			</Row>
		</Container>
	)
}
export default Highlights;