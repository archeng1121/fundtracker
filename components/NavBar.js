
import { useState , useContext } from 'react'
//lets acquire the context object that determines the value of the user.
import UserContext from '../UserContext'; 
import { Navbar , Container , Nav } from 'react-bootstrap';
//lets acquire The routing components of next JS
import Link from 'next/link'
import Router from 'next/router'



export default function NavBar() {
    const [isExpanded, setIsExpanded] = useState()

    //lets destructure the context object and acquire the values/components you want to consume. 
    const { user } = useContext(UserContext)
    
    //lets create a ternary structure for us to be able to determine what element can be seen if user is mounted.
    return(
        // lets render the navbar component as routing components
        <Navbar expanded={isExpanded} expand="lg" variant="dark" bg="dark" fixed="top">
            <Container>
                <Link href="/">
                    <a className="navbar-brand">MoneyTrack</a>
                </Link>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <>
                        {(user.email !== null)
                            ?
                            <>
                                <Link href="/user/records">
                                    <a className="nav-link">Records</a>
                                </Link>

                                <Link href="/user/categories">
                                    <a className="nav-link">Categories</a>
                                </Link>

                                <Link href="/user/charts/category-breakdown">
                                    <a className="nav-link">Charts</a>
                                </Link>
                                
                                <Link href="/logout">
                                    <a className="nav-link">Logout</a>
                                </Link>
                            </>
                   
                            :
                            <>
                                
                                <Link href="/login">
                                    <a className="nav-link">Login</a>
                                </Link>
                               
                                <Link href="/register">
                                    <a className="nav-link">Register</a>
                                </Link>


                            </>
                        }
                        </>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
} 
