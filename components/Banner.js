
// lets practice creating components using JSX syntax
// purpose : to be the hero section of our page.
//i'll be using a named export method to acquire my components from react-bootstrap
import { Jumbotron , Container , Button } from 'react-bootstrap' 

// let's acquire the bootstrap grid system
import { Row , Col } from 'react-bootstrap' 

// lets acquirea link component directly from next js
import Link from 'next/link'


let name = "Apple Cheng";

// our new task is to pass down props inside our Banner component


// i will create a function that will return the structure of my component
// for this example, let's create this function in a es6 format
const Banner = ({data}) => {
	// lets declare a return scope to determine the anatomy of the element

	// lets destructure the data prop into its properties
	const {title, content, destination, label} = data
	return(
		<>

			<Jumbotron>
				<h1>{title}</h1>
				<p>{content}</p>
				
				<Link href={destination}>
					<a>{label}</a>
				</Link>
			</Jumbotron>
	
		</>
	)
}

export default Banner;
