import { useState, useEffect } from 'react'
import { InputGroup, Form, Col, Button } from 'react-bootstrap'
import { Pie } from 'react-chartjs-2'
import moment from 'moment'
import View from '../../../components/View'
import AppHelper from '../../../app-helper'


const categoryBreakdown = (e) => {
   
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])
    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))

    const data = {
        labels: labelsArr,
        datasets: [
            {
                data: dataArr,
                backgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#FED6AA',
                    '#E4E6EB',
                    '#CD5C5C',
                    '#78C2AD',
                    '#FDC0B6',
                    '#AF86BC',
                    '#948779'
                ],
                hoverBackgroundColor: [
                    '#FF6384',
                    '#36A2EB',
                    '#FFCE56',
                    '#FED6AA',
                    '#E4E6EB',
                    '#CD5C5C',
                    '#78C2AD',
                    '#FDC0B6',
                    '#AF86BC',
                    '#948779'
                ]
            }
        ]
    };

    useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            },
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }
        console.log(AppHelper.API_URL)
        fetch('https://fathomless-brook-91469.herokuapp.com/api/users/get-records-breakdown-by-range', payload)
        .then(AppHelper.toJSON)
        .then(records => {
            console.log(records)
            // ******* This will change the values of the component base from tge information in the database ******** //
            setLabelsArr(records.map(record => record.categoryName))
            setDataArr(records.map(record => record.totalAmount))
        })
    },[fromDate, toDate])

       

    return (
        <Form className="mt-5 pt-3" onSubmit = {(e) => categoryBreakdown(e)}>
            <Form.Group controlId="category-breakdown">
                <Form.Control type="date" value={ fromDate } onChange={ (e) => setFromDate(e.target.value) }/>
                <Form.Control type="date" value={ toDate } onChange={ (e) => setToDate(e.target.value) }/>
                <Pie data={ data } />
            </Form.Group>
        </Form>
    )
}
export default categoryBreakdown;