import { Card, Button, Row, Col, InputGroup, FormControl, Form , Table } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../app-helper';
import { useState, useEffect} from'react';
import moment from 'moment';



const recordIndex = (props) => {
    
    const [data, setData] = useState(null)
    useEffect ( async () => {
        if(AppHelper.getAccessToken() == undefined) {
            return;
        }
        const payload = {
            method : 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }
        }
        // console.log(AppHelper.API_URL)
        const res = await fetch('https://fathomless-brook-91469.herokuapp.com/api/users/get-records', payload)
        const records = await res.json()
        console.log(records)
        setData( records )

        
    },[])

    return (
        <View title="Records">
            <h3>Records</h3>
            <InputGroup className="mb-2">
                <InputGroup.Prepend>
                    <Link href="/user/records/new">
                        <a className="btn btn-success">Add</a>
                    </Link>
                </InputGroup.Prepend>
                <FormControl placeholder="Search Record" />
                <Form.Control as="select">
                    <option value="All">All</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </Form.Control>
            </InputGroup>

            {data ? data.map(record => (<RecordsView key={record['_id']} data={record} />)) : <> </> }
            {/*{data ? data.map(record => (<RecordsView data={record}/>)) : <> </> }*/}
            {/*<RecordsView />*/}
        </View>
    )
}
export default recordIndex;

const RecordsView = ({data}) => { 
    console.log(data)
    
    return (
        <>
            {/*<h1>Balance : {data.balanceAfterTransaction}</h1>*/}
            <Card className="mb-3" border="secondary">
                <Card.Body>
                    <Row>
                        <Col xs={ 6 }>
                            <h5>{data.description}</h5>
                            <h6>
                              <span>{data.type} ({data.categoryName})</span> 
                            </h6>
                            {/*<p>{data.dateAdded}</p>*/}
                            <p>{moment(data.dateAdded).format("L")}</p>
                        </Col>
                        <Col xs={ 6 } className="text-right">
                            <h6>{data.amount}</h6>
                            <span>Balance : {data.balanceAfterTransaction}</span>
                        </Col>
                    </Row>
                </Card.Body>
            </Card>
        </>
    )
}


// <>
//     <Table responsive="sm">
//         <thead>
//             <tr>
//                 {/*<th>#</th>*/}
//                 <th>CATEGORY</th>
//                 <th>DESCRIPTION</th>
//                 <th>AMOUNT</th>
//                 <th>BALANCE</th>
//             </tr>
//         </thead>
//         <tbody id="recordBody">
//             <tr>
//                 {/*<td>#</td>*/}
//                 <td>{data.categoryName}</td>
//                 <td>{data.description}</td>
//                 <td>{data.amount}</td>
//                 <td>{data.balanceAfterTransaction}</td>
//             </tr>
//         </tbody>
//     </Table>
// </>

