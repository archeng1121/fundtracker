import { Table, Button } from 'react-bootstrap'
import Link from 'next/link'
import View from '../../../components/View'
import AppHelper from '../../../app-helper';
import { useState, useEffect} from'react';

const categoryIndex = (props) => {
    const [data, setData] = useState(null)
    useEffect ( async () => {
        if(AppHelper.getAccessToken() == undefined) {
            return;
        }
        const payload = {
            method : 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${ AppHelper.getAccessToken() }`
            }
        }
        console.log(AppHelper.API_URL)
        const res = await fetch('https://fathomless-brook-91469.herokuapp.com/api/users/get-categories', payload)
        const records = await res.json()
        console.log(records)
        setData( records )

        
    },[])

    return (
        <View title="Categories">
            <h3>Categories</h3>
            <Link href="/user/categories/new">
                <a className="btn btn-success mt-1 mb-3">Add</a>
            </Link>
            <Table striped bordered hover responsive="sm" border="secondary">
                <thead>
                    <tr>
                        <th>Category</th>
                        <th>Type</th>
                    </tr>
                </thead>
                <tbody>
                    {data ? data.map(record => (<RecordsView key={record['_id']} data={record}/>)) : <> </> }
                </tbody>
            </Table>
        </View>
    )
}
export default categoryIndex;

const RecordsView = ({data}) => { 
    console.log(data)
    
    return (
        <>
            <tr>
                <td>{data.categoryName}</td>
                <td>{data.type}</td>
            </tr>
        </>
    )
}
