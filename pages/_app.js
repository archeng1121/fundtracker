import { useState, useEffect } from 'react'
// import "bootswatch/dist/Minty/bootstrap.min.css";
import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'
import { Container } from 'react-bootstrap'

// lets make this document as our provider component.
import { UserProvider } from '../UserContext';

function MyApp({ Component, pageProps }) {
	// what are the info/data will our provider give to the consumers?
	// lets declare the component/variable which will describe the state of the user
	// we will initializethe user as an object
	const [user, setUser] = useState({
		email : null,
		isAdmin : null
	})

	// lets use the effect hook for us to be able to change the valueof our user variable/component. what values do we want to assign as the 
	useEffect( () => {
		setUser({
			email :localStorage.getItem('email'),
			isAdmin : localStorage.getItem('isAdmin') === 'true'
		})
	}, []) //an optional parameter

	// lets create another effect hook for testing the setUser() functionality
	// useEffect(() => {
	// 	// to test the changes, kets display the value of the user variable inside the console
	// 	console.log(`Si ${user.email} ang naka login.`)
	// }, [user.email]) //we are creating this for testing purposes only. (you may ommit this in the future)

	// lets create a logic that wil allow us to cear the contents of the localStorage which will be beneficial upon the usser logging out our application.
	const unsetUser = () => {
		// clear out the contents of the local storage
		localStorage.clear()
		// simply reset back the user components to its default states/values
		setUser({
			email : null,
			isAdmin : null
		})
	}

  	return (
	  	<>
	  		<UserProvider value={{ user, setUser, unsetUser }}>
	  			<NavBar />
		  		<Container>
		  			<Component {...pageProps} />
		  		</Container>
		  		<Footer />
	  		</UserProvider>
	  	</>
	) 

}

export default MyApp
