import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import { GoogleLogin } from "react-google-login"
import { useState, useEffect, useContext } from 'react'
import UserContext from '../../UserContext'

// lets intregate the particles-bg dependency
import dynamic from 'next/dynamic'
const ParticlesBg = dynamic(() => import("particles-bg"), { ssr : false })

// lets acquire our View component
import View from '../../components/View'

// lets aquire our helper document
import AppHelper from '../../app-helper.js'
import Router from 'next/router'

export default function index() {
  return(
    <View title={'Login'}>
      <Row className="justify-content-center">
        <Col xs md="6">
          <Login />
        </Col>
      </Row>
      <ParticlesBg type="cobweb" bg={true} />
    </View>
    
  )
}
  
function Login() {         
  // lets consume the values provided by the context object
  const { user, setUser } = useContext(UserContext);

  // lets define a state for out token Id
  const [tokenId, setTokenId] = useState(null);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const authenticate = (e) => { 
    e.preventDefault() //to avoid page redirection
    // console.log(e)
    localStorage.setItem('email', email)
    


    // practice task, lets refactor the current structure of our fetch requst by storing the payload inside an object
    const laman = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({
        email: email,
        password: password
      })
    }

    fetch('https://fathomless-brook-91469.herokuapp.com/api/users/login', //{
      // method: 'POST',
          // headers: {
          //    'Content-Type': 'application/json'
          // },
          // body: JSON.stringify({
          //    email: email,
         //     password: password
          // })
    //}
    laman)
    .then(AppHelper.toJSON)
    .then(data => {
      // console.log(data)
      //-> checker
      // lets create a controlstructure that will determine the response to the user.
      if (typeof data.accessToken !== "undefined") {
        // next task is to save theaccess token inside our local storage object
        // console.log(data)  

        setEmail(data.email)
        localStorage.setItem('token', data.accessToken)
        localStorage.setItem('email', email)
        console.log(email)
        // setEmail({
        //  email: localStorage.getItem('email')
        // })


        retrieveUserDetails(data.accessToken)
        // Swal.fire({
        //  icon: "success",
        //  title: "Successfully Logged In",
        //  text: "Congrats!"
        // })
      } 

      else {
        // inside this branch, we are going to give it these exact conditions upon failure in logging in.
        if (data.error === 'does-not-exist') {
          Swal.fire('Authentication Failed', 'User Does Not Exist', 'error')
        }
        else if(data.error === 'incorrect-password') {
          Swal.fire('Authentication Failed', 'Password is Incorrect', 'error')
        }
        else if(data.error === 'login-type-error') {
          Swal.fire('Authentication Failed', 'You may have registered using a diferent method, try using an aternative login method', 'error')
        }
        // Swal.fire({
        //  icon: "error",
        //  title: "Login Error",
        //  text: "You may have registered using a different Login Procedure."
        // })
      }
    })
  }

  // lets create a function that will allow us to retrieve the information or details about the user.
  // upon retrieving the info of the user, the user has to be authenticated first
  const retrieveUserDetails = (accessToken) => {
    // lets create an object whichwe will name as option, and the value it holds is the access token
    const options = {
      headers : { Authorization : `Bearer ${accessToken}` }
    } //this will serve as the payload of the request.

    // send request together with the payload
    fetch('https://fathomless-brook-91469.herokuapp.com/api/users/details', options)
    .then(AppHelper.toJSON)
    .then(data => {
      // change the value of the user component by targeting it's state setter
      setUser({ id : data._id , isAdmin : data.isAdmin })
      // lets redirect the user inside the courses pages.
      Router.push('/user/records');
    })
  
  }

  //lets create a function that will allow us to communicate with google and authenticate the user by generating an accessToken
  //create a parameter that will describe the response of google.
  const authenticateGoogleToken = (response) => {
    //lets create a checker for us to be able to catch first the response from google.
    console.log(response)
    setTokenId(response.tokenId)
    setEmail(response.profileObj.email)
    localStorage.setItem('email', response.profileObj.email)
    // console.log(set.Email)

    //simply save the accesstoken inside our local storage
    localStorage.setItem('googleToken', response.tokenId)
    //send a request to our backend project which will allow you to get the information about the user.
    const payload = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ tokenId: response.tokenId })
    }

    //since we have created the payload structure
    fetch('https://fathomless-brook-91469.herokuapp.com/api/users/verify-google-id-token', payload)
    .then(AppHelper.toJSON).then(data => {
      console.log(data)
      //lets create a control structure to determine the procedure when receiving the response. 
      if (typeof data.accessToken !== 'undefined') {
        //save the access token inside the local storage
        localStorage.setItem('token', data.accessToken)
        retrieveUserDetails(data.accessToken)
      }

      else {
        //what will be the response if the token id from google was not verified?
        if (data.error == 'google-auth-error') {
          Swal.fire(
            'Google Auth Error', 
            'Google Authentication Failed', 
            'error'
          )
        }
        else if(data.error === 'login-type-error') {
          Swal.fire(
            'Google Auth Error', 
            'You might have registered through a different login procedure.', 
            'error'
          )
        }
      }
    })
  }



  return(
    <Container className="login mt-3">
      <h1 className="m-1">Login Page</h1>
      <Form onSubmit={(e) => authenticate(e)} >
        {/*email*/}
        <Form.Group controlId="email">
          <Form.Label>Email:</Form.Label>
          <Form.Control value={email} placeholder="Insert Email Address Here" onChange={e => setEmail(e.target.value)} type="email" required />
        </Form.Group>
        {/*Password*/}
        <Form.Group controlId="password">
          <Form.Label>Password:</Form.Label>
          <Form.Control value={password} placeholder="Insert Password Here" onChange={e => setPassword(e.target.value)} type="password" required />
        </Form.Group>
        <Button  type="submit" className="btn btn-lg btn-primary w-100 mt-3 mb-3">Login</Button>
        <GoogleLogin
          /*clientId="822919040766-5fil3fn2eqr5bd58ql1kji303bm25hec.apps.googleusercontent.com"*/
          clientId="324468674788-78vf8css0olih1hekul22pju2u8emmn7.apps.googleusercontent.com"
          className="w-100 text-center d-flex justify-content-center mt-3" 
          buttonText="Login"
          onSuccess={ authenticateGoogleToken }
          //callback function that is run on success
          onFailure={ authenticateGoogleToken }
          //callback function that is run on failure
          cookiePolicy={ 'single_host_origin' }

        />
      </Form>
    </Container>

  )
}

  
  
